FROM ubuntu:16.04
USER root
ENV DEBIAN_FRONTEND=noninteractive JAVA_HOME=/usr/lib/jvm/java-8-oracle TIGERVNC_VER=tigervnc-1.8.0.x86_64
ARG VNC_PASSWORD=rsbotpas

# update package list and install packages
RUN apt-get update && apt-get upgrade -y && apt-get install -y --no-install-recommends \
curl software-properties-common i3 xvfb libxtst6 libxrender1 libxi6 wget openjdk-8-jdk scrot fluxbox compton 

# Install java oracle 8
#RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections \
#&& apt-add-repository -y ppa:webupd8team/java \
#&& apt-get update \
#&& apt-get install -y oracle-java8-installer

# Download tigervnc
RUN curl -L \
https://bintray.com/tigervnc/stable/download_file?file_path=$TIGERVNC_VER.tar.gz \
> /root/$TIGERVNC_VER.tar.gz && tar xf /root/$TIGERVNC_VER.tar.gz -C /root/

# Configure tigervnc
RUN mkdir /root/.vnc && printf "$VNC_PASSWORD\n$VNC_PASSWORD\n\n" | /root/$TIGERVNC_VER/usr/bin/vncpasswd \
&& echo -e '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\ncompton &' > /root/.vnc/xstartup \
&& chmod u+x /root/.vnc/xstartup && rm /etc/machine-id

COPY start.sh /root/start.sh

RUN chmod u+x /root/start.sh

EXPOSE 5900

ENTRYPOINT ["/bin/bash", "-c", "source /etc/environment && /root/start.sh 2>&1 | tee output.txt"]
