#!/bin/bash
export DISPLAY=:0
export TIGERVNC_VER=tigervnc-1.8.0.x86_64

function client_download {
    echo "Downloading powerbot"
    
    STATUSCODEPOWERBOT=$(wget -N -L 'https://www.powerbot.org/download/?jar')
    FILESIZE=$(stat -c%s "/root/index.html?jar")
    if [ "$FILESIZE" -lt 1001 ]
    then
        sleep 900
        client_download
    fi
}

function jagexclient_download {
    echo "Downloading jagex appletviewer"
    STATUSCODE_RS=$(wget -N -L 'http://www.runescape.com/downloads/jagexappletviewer.jar')
    
    FILESIZE=$(stat -c%s "/root/jagexappletviewer.jar")
    # is around 60kb in size
    if [ "$FILESIZE" -lt 50000 ]
    then
        sleep 900
        jagexclient_download
    fi
}

function check_updates {
    cd /root
    client_download
    jagexclient_download
}

function setup_vnc_password {
    # Configure tigervnc
    echo "configuring tigervnc..."
    rm -rf /root/.vnc && mkdir /root/.vnc && printf "$VNC_PASSWORD\n$VNC_PASSWORD\n\n" | /root/$TIGERVNC_VER/usr/bin/vncpasswd \
    && echo -e '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\ncompton &' > /root/.vnc/xstartup \
    && chmod u+x /root/.vnc/xstartup
}

function check_running {
    countprocesses=$(pgrep -f runescape.com | wc -l)
    if [ $countprocesses -lt 1 ]
    then
        start_client
    fi
}

function start_client {
    java -Dcom.jagex.config=http://oldschool.runescape.com/l=en/jav_config.ws -Xmx450M -Xss228K -XX:CompileThreshold=1500 -Xincgc -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -javaagent:/root/index.html?jar -Djava.search.path=/root/powerbotscripts/out -Dfile.encoding=UTF-8 -jar /root/jagexappletviewer.jar runescape &
}

function initialise_environment {
    source /root/.profile
    env
    rm -rf /tmp/.*
    if [ ! -z ${VNC_PASSWORD+x} ]
    then
        setup_vnc_password
    fi
    /root/$TIGERVNC_VER/usr/bin/vncserver :0 &
    sleep 5
    xcompmgr &
    fluxbox &
    cd /root/
    check_updates
    start_client
}

function main {
    initialise_environment
    check_updates
    sleep 900
    
    # loop every 15 minutes
    for i in {1..100000}
    do
        check_updates
        check_running
        sleep 900
    done
}
main
shutdown -h 0
